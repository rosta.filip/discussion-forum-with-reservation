function createTimeTable() {
    $(document).ready(function () {

        $('#id-tab-timetable').DataTable(
            {
                "paging":false,
                "searching":false,
                "ordering":false,
            }
        );
        $('td#timetable-cell').click(
            function (e) {
                e.preventDefault();
                window.location.replace($(this).data('link'));
            }
        );
    });
}

createTimeTable();


function itemDetail() {
    $(document).ready(function () {

        $('div#item_view').click(
            function (e) {
                e.preventDefault();
                window.location.replace($(this).data('link'));
            }
        );
    });
}

itemDetail();


/*
const days = ['Ne', 'Po','Ut','St','Ct','Pa','So']

function generateWeekTable(weekNumber, reservationUrl) {
    let tableDataParent = document.getElementById('id-tab');
    let tableHeader;
    let tableRow;
    let tableData;

    let today = new Date()
    let dateShift = 7 * weekNumber;
    let firstHour = 6;
    let lastHour = 22;

    tableRow= document.createElement( 'tr');
    tableDataParent.appendChild(tableRow);
    tableHeader= document.createElement( 'th');
    tableRow.appendChild(tableHeader);

    for (let i = 0; i <= 6; i++) {
        tableHeader = document.createElement("th");
        tableRow.appendChild(tableHeader);

        let date = new Date(today);
        date.setDate(date.getDate() + i + dateShift);
        let formatted = `${days[date.getDay()]} ${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()}`;
        tableHeader.innerHTML="<th>" + formatted + "</th>";
    }

    for (let i = firstHour; i <= lastHour; i += 2) {
        tableRow = document.createElement("tr");
        tableDataParent.appendChild(tableRow);
        tableHeader = document.createElement("th");
        tableRow.appendChild(tableHeader);
        let time = `${i}:00`;
        tableHeader.innerHTML="<th>" + time + "</th>";

        for (let j=0; j <= 6; j++) {
            let date = new Date(today);
            date.setDate(date.getDate() + i + dateShift);
            let formatted = `${days[date.getDay()]} ${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()}`;

            tableData = document.createElement("td");
            tableRow.appendChild(tableData);
            tableData.innerHTML=`<td><a class="btn btn-secondary" href="${reservationUrl}?datetime=${formatted} ${time}">reservation</a></td>`;
        }
    }
} */
