from enum import IntEnum


class Turns(IntEnum):
    morning = 10
    afternoon = 20
    evening = 30


class Items(IntEnum):
    ice_axe = 10
    crampons = 20
    harness = 30
    helmet = 40


class Sizes(IntEnum):
    child = 10
    small = 20
    medium = 30
    large = 40
    extra_large = 50
