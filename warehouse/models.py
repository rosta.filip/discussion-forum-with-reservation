from django.db.models import (
    CharField,
    DateField,
    TextField,
    IntegerField,
    ImageField,
    TimeField,
    EmailField,
    BooleanField,
    ForeignKey,
    Model,
    SmallIntegerField,
    PROTECT,
    CASCADE,
)


# from warehouse.choices import ITEMS_TYPES, ITEMS_SIZES
from django.utils.html import mark_safe


class TimeTable(Model):
    date = DateField(auto_now=False, auto_now_add=False, null=False, blank=False)  # YYYY-MM-DD format
    start_time = TimeField(auto_now=False, auto_now_add=False, null=False, blank=False)
    end_time = TimeField(auto_now=False, auto_now_add=False, null=False, blank=False)
    #    turn = SmallIntegerField(choices=TURNS_TYPES, null=False, blank=False)
    capacity = SmallIntegerField(null=False, blank=False)

    @property
    def date_stime(self):
        return f"{self.date} {self.start_time}"

    def __str__(self):
        return f'{self.date} ({self.start_time})'

    def __repr__(self):
        return f'{self.date} ({self.start_time})'


class Category(Model):
    #    category = SmallIntegerField(choices=ITEMS_TYPES, null=False, blank=False)
    category = CharField(max_length=64, null=False, blank=False)

    def __str__(self):
        return f'{self.category}'


class Manufacturer(Model):
    m_name = CharField(max_length=64, null=False, blank=False)
    country = CharField(max_length=64, null=False, blank=False)
    m_description = TextField(blank=True)

    def __str__(self):
        return f"{self.m_name}"


class Item(Model):
    category = ForeignKey(Category, null=False, on_delete=PROTECT, related_name='items')
    name = CharField(max_length=64, null=False, blank=False)
    manufacturer = ForeignKey(Manufacturer, null=False, on_delete=PROTECT, related_name='manufacturers')
    #    size = SmallIntegerField(choices=ITEMS_SIZES, null=False, blank=False)
    stock = SmallIntegerField(null=False, blank=False)
    description = TextField(blank=True)
    image = ImageField(upload_to='product_image') # pokud stači jen jedna fotka -> jinak musi byt samostatná tabulka pro IMG
    is_active = BooleanField(default=True)
    price = IntegerField(null=False, blank=False)

    @property
    def item_description(self):
        return (
            f"{self.name} {self.image}, {self.manufacturer}, {self.category}"
            f"{self.stock}, {self.description}"
        )

    def __str__(self):
        return f'{self.name} - {self.category}'


class Reservation(Model):
    first_name = CharField(max_length=64, null=False, blank=False)
    last_name = CharField(max_length=64, null=False, blank=False)
    phone_number = CharField(max_length=32, default='', null=False, blank=False)
    email = EmailField(max_length=128, default='', null=False, blank=False)
    person_quantity = SmallIntegerField(null=False, blank=False)
    route_numbers = SmallIntegerField(null=False, blank=False)
    timeslot = ForeignKey(TimeTable, null=False, on_delete=PROTECT, related_name='reservations')

    @property
    def full_name(self):
        return f"{self.last_name} {self.first_name}"

    def __str__(self):
        return f'{self.full_name} [{self.timeslot}]'


class ReservationItems(Model):
    reservation = ForeignKey(Reservation, on_delete=CASCADE, related_name='items')
    item = ForeignKey(Item, null=False, on_delete=PROTECT) # related_name='reserved_item' -> zatim bez tohoto, mozna nebudu potrebovat..
    quantity = SmallIntegerField(null=False, blank=False)


class Banners(Model):
    image = ImageField(upload_to='images/banners/')
    text = CharField(max_length=256)

    def __str__(self):
        return self.text

    def image_tag(self):
        return mark_safe('<img src="%s" width="80" />' % (self.image.url))
