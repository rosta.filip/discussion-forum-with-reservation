from django.core.exceptions import ValidationError

from django.forms import ModelForm, CharField, EmailField, IntegerField, ModelChoiceField, HiddenInput, Textarea, \
    ImageField, BooleanField, ChoiceField

from warehouse.models import Reservation, TimeTable, ReservationItems, Item


def first_name_many_words(value: str):
    if len(value.split()) > 1:
        raise ValidationError('First name must contain only one word.')


def proper_phone_number(value: str):
    if value[0] != '+':
        raise ValidationError('Enter the valid number with the prefix')


def min_people(value: int):
    if value < 2:
        raise ValidationError('They must be at least 2 people to enter.')


def max_people(value: int):
    if value > 30:
        raise ValidationError("It's too many people (max capacity is 30 people).")


class ReservationForm(ModelForm):

    class Meta:
        model = Reservation
        fields = ('first_name', 'last_name', 'phone_number', 'email', 'person_quantity', 'route_numbers', 'timeslot')

    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('route_numbers')
        super(ReservationForm, self).__init__(*args, **kwargs)
        self.fields['route_numbers'].choices = choices

    first_name = CharField(
        min_length=2,
        max_length=64,
        validators=[first_name_many_words],
        required=True,
        label='Jméno')
    last_name = CharField(min_length=2, max_length=64, required=True, label='Příjmení')
    phone_number = CharField(
        max_length=32,
        validators=[proper_phone_number],
        required=False,
        label='Telefoní číslo')
    email = EmailField(max_length=128, required=True, validators=[])
    person_quantity = IntegerField(min_value=1, max_value=30, label='Počet osob', validators=[min_people, max_people])
    route_numbers = ChoiceField(
        choices='choices',
        label='Počet lan')
    timeslot = ModelChoiceField(queryset=TimeTable.objects, required=True, widget=Textarea(attrs={'hidden': ''}), label='')

    def get_initial_for_field(self, field, field_name):
        if field_name == 'timeslot':
            return self.initial[field_name]
        return super().get_initial_for_field(field, field_name)

    def clean_first_name(self):
        return self.cleaned_data['first_name'].lower().capitalize()

    def clean_last_name(self):
        return self.cleaned_data['last_name'].lower().capitalize()

    def clean_email(self):
        return self.cleaned_data['email'].lower()

    def clean(self):
        cleaned_data = super().clean()
        people = cleaned_data.get("person_quantity")
        ropes = cleaned_data.get("route_numbers")

        if people and ropes:
            if int(int(people) / int(ropes)) < 2:
                raise ValidationError("Not enough people for this amount of ropes.")


class ReservationItemsForm(ModelForm):
    class Meta:
        model = ReservationItems
        widgets = {
            'reservation': HiddenInput(),
        }
        fields = ('reservation', 'item', 'quantity')

    reservation = ModelChoiceField(queryset=Reservation.objects, required=True, widget=Textarea(attrs={'hidden': ''}), label='')
    item = ModelChoiceField(queryset=Item.objects, label='Co chceš půjčit')
    quantity = IntegerField(min_value=1, max_value=30, label='Množství')

    def get_initial_for_field(self, field, field_name):
        if field_name == 'reservation':
            return self.initial[field_name]
        return super().get_initial_for_field(field, field_name)


class CategoryForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'

    category = CharField(max_length=64)


class ManufacturerForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'

    m_name = CharField(max_length=64)
    country = CharField(max_length=64)
    m_description = CharField


class WarehouseReviewForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'

    category = CharField(max_length=64)
    manufacturer = CharField(max_length=64)

    name = CharField(max_length=64)
    stock = IntegerField()
    description = CharField()
    image = ImageField(max_length=128, required=True)
    is_active = BooleanField
    price = IntegerField

    def get_initial_for_field(self, field, field_name):
        if field_name == 'category':
            return self.initial[field_name]
#        elif field_name == 'manufacturer':
#            return self.initial[field_name]
        return super().get_initial_for_field(field, field_name)
