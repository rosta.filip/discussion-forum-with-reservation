from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import path
from django.views.generic import RedirectView

from warehouse.views import ReservationCreateView, ReservationItemsCreateView, WeekTableView, SuccessView, RentalDetailView, \
    RentalViewFilter

app_name = 'warehouse'

urlpatterns = [
    # Timetable for reservation
    path('week_table/', WeekTableView.as_view(), name='week_table'),

    # Create reservation
    path('reservation_create/<slug:requested_date>/<str:requested_time>/<str:capacity>/', ReservationCreateView.as_view(), name='reservation_create'),
    # Add item to a reservation
    path('item_reservation/<int:reservation>/', ReservationItemsCreateView.as_view(), name='item_reservation'),
    # Confirmation of reservation
    path('reservation_success/<int:reservation>/', SuccessView.as_view(), name='reservation_success'),

    # Rental review
    path('rental_list/', RentalViewFilter.as_view(), name='rental_list'),
    # Rental detail
    path('rental_detail/<int:pk>/', RentalDetailView.as_view(), name='rental_detail'),

    # Favicon
    # path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('favicon/favicon.ico'))),
]
