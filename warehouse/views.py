from datetime import datetime, timedelta, time

from django.db.models import Sum
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, CreateView, DetailView, ListView

from warehouse.forms import ReservationForm, ReservationItemsForm
from warehouse.models import TimeTable, Reservation, Item, Manufacturer, Category

from django.conf import settings
from django.core.mail import send_mail


class IndexView(TemplateView):
    """ Home page. """
    template_name = "index.html"


class WeekTableView(TemplateView):
    """ Timetable for reservation. """
    template_name = 'week_table.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # for how many weeks do you need a reservation
        context['max_week'] = 3
        context['current_week'] = int(self.request.GET.get('week_number', 0))
        context['prev_week'] = context['current_week'] - 1
        context['next_week'] = context['current_week'] + 1

        # setting a day
        dateshift = 7 * context['current_week']
        today = datetime.today().date()
        day_1 = today + timedelta(days=dateshift)
        day_7 = today + timedelta(days=7+dateshift)
        current_date = day_1

        # Creating a timetable for reservation

        # names of days in first row (creating a variable header)
        header = []
        for _ in range(7):
            header.append({
                "name": current_date.strftime('%d-%m-%y%n\n%A%n'),
                "date": current_date
                })
            current_date += timedelta(days=1)

        # sum of reservation for current week (creating a variable week_reservations)
        week_reservations = Reservation.objects.filter(
            timeslot__date__gte=day_1,
            timeslot__date__lte=day_7,
            ).values('timeslot__date', 'timeslot__start_time'
            ).annotate(route_sum=Sum('route_numbers')
            ).values('route_sum', 'timeslot__date', 'timeslot__start_time')

        # values from timetable for current week (creating a variable timetable)
        timetable = TimeTable.objects.filter(
            date__gte=day_1,
            date__lte=day_7,
        )

        # variables for holding values for every cell in timetable for current_week
        week_map = {}
        week_capacity = {}

        # add every event from timetable for current_week (variable timetable) to new variable (week_map)
        # select necessary values (event.date and event.start_time)
        for event in timetable:
            week_map[(event.date, event.start_time)] = event

        # add every event from week_reservation for current_week (variable week_resertvations)
        # to new variable (week_capacity)
        # select necessary values
        for space in week_reservations:
            week_capacity[(space['timeslot__date'], space['timeslot__start_time'])] = space['route_sum']

        # hours range
        start_hour = 6
        end_hour = 23

        # names of hours in first column (setting a step -> 2 hours) -> current hour
        # (creating a variable rows)
        rows = []
        for hour in range(start_hour, end_hour, 2):
            current_hour = time(hour=hour, minute=0)

            # create a variable days (holding values for cell - date, timetable, capacity)
            days = []
            for current_date in header:
                day = {
                    "date": "",
                    "timetable": week_map.get(
                        (current_date["date"], current_hour), None
                    ),
                    "capacity": week_capacity.get(
                        (current_date["date"], current_hour), 0
                    ),
                }
                days.append(day)

            rows.append({
                "hour": current_hour.strftime("%H:%M"),
                "days": days
            })

        context.update({
            "header": header,
            "rows": rows
        })

        return context


class ReservationCreateView(CreateView):
    """ Create reservation """
    template_name = 'reservation.html'
    form_class = ReservationForm
    success_url = reverse_lazy('warehouse:item_reservation')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['timetable'] = TimeTable.objects.filter(
            date=self.kwargs['requested_date'],
            start_time=self.kwargs['requested_time']
        ).first()
        context['capacity'] = context['timetable'].reservations.aggregate(Sum('route_numbers'))['route_numbers__sum']
        return context

    def get_initial(self):
        init_data = super().get_initial()
        init_data['timeslot'] = TimeTable.objects.filter(
            date=self.kwargs['requested_date'],
            start_time=self.kwargs['requested_time']
        ).first()
        return init_data

    def get_form_kwargs(self):
        kwargs = super(ReservationCreateView, self).get_form_kwargs()
        req_time = TimeTable.objects.filter(
            date=self.kwargs['requested_date'],
            start_time=self.kwargs['requested_time']
        ).first()
        occupied_ropes = req_time.reservations.aggregate(Sum('route_numbers'))['route_numbers__sum']
        if occupied_ropes is None:
            occupied_ropes = 0
        full_capacity = req_time.capacity
        choice = (full_capacity - occupied_ropes)
        choices = [x for x in range(1, choice+1)]  # tuple(map(lambda x: (int(x), int(x)), choices))
        kwargs['route_numbers'] = tuple(map(lambda x: (int(x), int(x)), choices))
        return kwargs

    def get_success_url(self):
        return reverse(
            'warehouse:item_reservation',
            kwargs={'reservation': self.object.id}
        )


class ReservationItemsCreateView (CreateView):
    """ Add Item to a reservation. """
    template_name = 'warehouse.html'
    form_class = ReservationItemsForm
    success_url = reverse_lazy('warehouse:reservation_success')

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['reservation'] = Reservation.objects.filter(
            id=self.kwargs['reservation'],
        ).first()
        return context

    def get_initial(self):
        init_data = super().get_initial()
        init_data['reservation'] = Reservation.objects.filter(
            id=self.kwargs['reservation'],
        ).first()
        return init_data

    def get_success_url(self):
        return reverse(
            'warehouse:reservation_success',
            kwargs={'reservation': self.object.reservation.id})


class SuccessView (TemplateView):
    """ Reservation confirmation - email. """
    template_name = "reservation_success.html"
    model = Reservation
    reservation_information = []

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['this_reservation'] = Reservation.objects.filter(
            id=self.kwargs['reservation'],
        ).first()

        subject = f'Reservation confirmation ({context["this_reservation"].timeslot})'
        message = f'Hi {context["this_reservation"].first_name} {context["this_reservation"].last_name}, ' \
                  f' thank you for your reservation for {context["this_reservation"].person_quantity} people ' \
                  f'({context["this_reservation"].route_numbers} rope/s) for {context["this_reservation"].timeslot}.' \
                  f'If you need to change or cancel your reservation call to +420 732 732 751.'
        email_from = 'rosta.filip@email.cz'
        recipient_list = [context["this_reservation"].email]
        # currently with # to stop sending emails during WIP...
        # send_mail(subject, message, email_from, recipient_list, fail_silently=False)
        return context


class RentalViewFilter(ListView):
    """ Rental - review page. """
    model = Item
    template_name = 'rental_review.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['item_list'] = Item.objects.all()
        context['manufacturer_list'] = Manufacturer.objects.all()
        context['category_list'] = Category.objects.all()

        context['current_manufacturer'] = int(self.request.GET.get('manufacturer_select', 0))
        context['current_category'] = int(self.request.GET.get('category_select', 0))

        context['current_manufacturer_name'] = Manufacturer.objects.filter(
            id=int(context['current_manufacturer']))
        context['current_category_name'] = Category.objects.filter(
            id=int(context['current_category']))

        context['manufacturer_filter_list'] = Item.objects.filter(manufacturer=context['current_manufacturer'])
        context['category_filter_list'] = Item.objects.filter(category=context['current_category'])
        context['combination_filter_list'] = Item.objects.filter(manufacturer=context['current_manufacturer']).filter(category=context['current_category'])

        return context


class RentalDetailView (DetailView):
    """ Rental - detailed information about product. """
    model = Item
    template_name = 'rental_detail.html'
    queryset = Item.objects.all()

