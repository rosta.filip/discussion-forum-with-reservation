from warehouse.enums import Turns, Items, Sizes

TURNS_TYPES = (
    (Turns.morning.value, Turns.morning.name),
    (Turns.afternoon.value, Turns.afternoon.name),
    (Turns.evening.value, Turns.evening.name),
)

ITEMS_TYPES = (
    (Items.ice_axe.value, Items.ice_axe.name),
    (Items.crampons.value, Items.crampons.name),
    (Items.harness.value, Items.harness.name),
    (Items.helmet.value, Items.helmet.name)
)

ITEMS_SIZES = (
    (Sizes.child.value, Sizes.child.name),
    (Sizes.small.value, Sizes.small.name),
    (Sizes.medium.value, Sizes.medium.name),
    (Sizes.large.value, Sizes.large.name),
    (Sizes.extra_large.value, Sizes.extra_large.name)
)
