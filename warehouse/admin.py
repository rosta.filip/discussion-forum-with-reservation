from django.contrib import admin
from django.contrib.admin import ModelAdmin, TabularInline

# Register your models here.

from warehouse.models import TimeTable, Category, Manufacturer, Item, Reservation, ReservationItems, Banners


class BannerAdmin(ModelAdmin):
    list_display = ('text', 'image')


class CategoryAdmin(ModelAdmin):
    list_display = ('id', 'category')


class ManufacturerAdmin(ModelAdmin):
    list_display = ('m_name', 'country')


class ItemsAdmin(ModelAdmin):
    list_display = ('category', 'manufacturer', 'name', 'stock', 'price')

    @admin.display(ordering='category__category')
    def category(self, obj):
        return obj.category.category

    @admin.display(ordering='manufacturer__m_name')
    def manufacturer(self, obj):
        return obj.item.manufacturer.m_name


class TimeTableAdmin(ModelAdmin):
    list_display = ('id', 'date', 'start_time', 'end_time', 'capacity')


class ReservationItemsAdmin(ModelAdmin):
    list_display = ('date_start_time', 'reservation_name', 'quantity', 'item_name')
    fieldsets = [
        ('ReservationItems',
         {'fields': ['quantity']}),
        ('Item',
         {'fields': [
             'item']}),
        ('Reservation',
         {'fields': [
             'reservation']}),
    ]

    @admin.display(ordering='item__name')
    def item_name(self, obj):
        return obj.item.name

    @admin.display(ordering='timetable__date_stime')
    def date_start_time(self, obj):
        return obj.reservation.timeslot.date_stime

    @admin.display(ordering='reservation__full_name')
    def reservation_name(self, obj):
        return obj.reservation.full_name


class ReservationAdmin(ModelAdmin):
    list_display = ('date_start_time', 'full_name', 'person_quantity', 'route_numbers')
    fieldsets = [
        ('Reservation',
         {'fields': ['first_name', 'last_name', 'phone_number', 'email', 'person_quantity', 'route_numbers']}),
        ('Timeslot',
         {'fields': [
             'timeslot']}),
    ]

    @admin.display(ordering='timetable__date_stime')
    def date_start_time(self, obj):
        return obj.timeslot.date_stime


admin.site.register(Category, CategoryAdmin)
admin.site.register(Manufacturer, ManufacturerAdmin)
admin.site.register(TimeTable, TimeTableAdmin)
admin.site.register(Banners, BannerAdmin)
admin.site.register(Item, ItemsAdmin)
admin.site.register(ReservationItems, ReservationItemsAdmin)
admin.site.register(Reservation, ReservationAdmin)
