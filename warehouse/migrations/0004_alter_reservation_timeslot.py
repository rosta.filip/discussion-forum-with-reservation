# Generated by Django 3.2.6 on 2021-08-21 19:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('warehouse', '0003_auto_20210813_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='timeslot',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='reservations', to='warehouse.timetable'),
        ),
    ]
