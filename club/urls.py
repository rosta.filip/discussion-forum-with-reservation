""" Defining URL patterns for registration. """

from django.urls import path

from club.views import ClubView, \
    ActualitiesView, ActualityDetailView, ActualityCreateView, ActualityEditView, \
    DiscussionsView, DiscussionDetailView, DiscussionCreateView, DiscussionEditView, DiscussionReactionView

app_name = 'club'

urlpatterns = [
    # Club main page
    path('', ClubView.as_view(), name='club'),

    # Actualities topics page
    path('actualities/', ActualitiesView.as_view(), name='actualities'),
    # Actuality detail
    path('actuality/<int:id>/', ActualityDetailView.as_view(), name='actuality'),
    # Create new actuality
    path('new_actuality/', ActualityCreateView.as_view(), name='new_actuality'),
    # Edit actuality
    path('edit_actuality/<int:id>/', ActualityEditView.as_view(), name='edit_actuality'),

    # Discussion themes page
    path('discussions/', DiscussionsView.as_view(), name='discussions'),
    # Discussion theme detail
    path('discussion/<int:id>/', DiscussionDetailView.as_view(), name='discussion'),
    # Create new discussion
    path('new_discussion/', DiscussionCreateView.as_view(), name='new_discussion'),
    # Edit discussion theme
    path('edit_discussion/<int:id>/', DiscussionEditView.as_view(), name='edit_discussion'),
    # Reaction on theme in discussion
    path('reaction_discussion/<int:id>/', DiscussionReactionView.as_view(), name='reaction_discussion'),

]