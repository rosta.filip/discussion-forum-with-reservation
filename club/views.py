from django.forms import HiddenInput
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, DetailView, ListView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q, Sum, Count

from club.models import Actuality, Discussion
from club.forms import ActualityCreateForm, DiscussionCreateForm, DiscussionReactionForm

from datetime import datetime

# Create your views here.


class ClubView(LoginRequiredMixin, ListView):
    """ The home page for Club. Showing last 2 new actualities and last 2 new themes in discussion. """
    template_name = 'club/club.html'
    model = Discussion

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['actualities'] = Actuality.objects.all().order_by('-created')[:2]
        context['discussions'] = Discussion.objects.filter(Q(reaction=None)).order_by('-posted')[:2]
        context['reactions'] = Discussion.objects.filter(~Q(reaction=None)).values('theme', 'reaction').annotate(
            Count('id')).filter(id__count__gt=0)
        return context


class ActualitiesView(LoginRequiredMixin, ListView):
    """ The page for displaying all Actualities. """
    template_name = 'club/actualities.html'
    model = Actuality

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['actualities'] = Actuality.objects.all().order_by('-created')
        return context


class ActualityDetailView(LoginRequiredMixin, DetailView):
    """The page for detail information about Actuality. """
    template_name = 'club/actuality.html'
    model = Actuality

    def get_object(self, queryset=None):
        actuality = Actuality.objects.get(id=self.kwargs['id'])
        return actuality


class ActualityCreateView(LoginRequiredMixin, CreateView):
    """ Create a new Actuality. """
    template_name = 'club/actuality_new.html'
    model = Actuality
    form_class = ActualityCreateForm
    # fields = ['topic', 'story', 'image', 'created', 'author']
    success_url = reverse_lazy('club:actualities')

    def get_initial(self):
        init_data = super().get_initial()
        init_data['author'] = self.request.user.id
        init_data['created'] = datetime.now()
        return init_data


class ActualityEditView(LoginRequiredMixin, UpdateView):
    """ The page for editing existing actuality. """
    template_name = 'club/actuality_edit.html'
    model = Actuality
    form_class = ActualityCreateForm
    success_url = reverse_lazy('club:actualities')

    def get_object(self, queryset=None):
        actuality = Actuality.objects.get(id=self.kwargs['id'])
        user = self.request.user
        if user == actuality.author:
            return actuality
        else:
            raise Http404


class DiscussionsView(LoginRequiredMixin, ListView):
    """ The page for displaying all themes from Discussions. """
    template_name = 'club/discussions.html'
    model = Discussion

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['discussions'] = Discussion.objects.filter(Q(reaction=None)).order_by('-posted')
        context['reactions'] = Discussion.objects.filter(~Q(reaction=None)).values('theme', 'reaction').annotate(
            Count('id')).filter(id__count__gt=0)

        return context


class DiscussionDetailView(LoginRequiredMixin, DetailView):
    """The page for detail information about Discussion. """
    template_name = 'club/discussion.html'
    model = Discussion
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['discussion'] = Discussion.objects.get(id=self.kwargs['id'])
        context['reactions'] = Discussion.objects.filter(reaction=self.kwargs['id']).order_by('-posted')
        context['sum_reactions'] = len(context['reactions'])
        return context


class DiscussionCreateView(LoginRequiredMixin, CreateView):
    """ Create a new Discussion theme. """
    template_name = 'club/discussion_new.html'
    model = Discussion
    form_class = DiscussionCreateForm
    success_url = reverse_lazy('club:discussions')

    def get_initial(self):
        init_data = super().get_initial()
        init_data['author'] = self.request.user.id
        init_data['posted'] = datetime.now()
        return init_data


class DiscussionEditView(LoginRequiredMixin, UpdateView):
    """ The page for editing existing discussion theme. """
    template_name = 'club/discussion_edit.html'
    model = Discussion
    form_class = DiscussionCreateForm
    success_url = reverse_lazy('club:discussions')

    def get_object(self, queryset=None):
        discussion = Discussion.objects.get(id=self.kwargs['id'])
        user = self.request.user
        if user == discussion.author:
            return discussion
        else:
            raise Http404


class DiscussionReactionView(LoginRequiredMixin, CreateView):
    """ Create a new Reaction on theme in discussion. """
    template_name = 'club/discussion_reaction.html'
    model = Discussion
    form_class = DiscussionReactionForm
    success_url = reverse_lazy('club:discussions')

    def get_initial(self):
        init_data = super().get_initial()
        discussion = Discussion.objects.get(id=self.kwargs['id'])
        init_data['theme'] = discussion.theme
        init_data['author'] = self.request.user.id
        init_data['posted'] = datetime.now()
        init_data['reaction'] = discussion.id
        return init_data
