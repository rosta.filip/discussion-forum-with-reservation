from django.forms import EmailField, ModelForm, CharField, DateField, ChoiceField, HiddenInput
from club.models import Actuality, Discussion


class ActualityCreateForm(ModelForm):
    """ For creating new actuality. """

    class Meta(ModelForm):
        model = Actuality
        fields = ['topic', 'story', 'image', 'created', 'author']
        widgets = {'created': HiddenInput(), 'author': HiddenInput()}


class DiscussionCreateForm(ModelForm):
    """ For creating new discussion theme. """

    class Meta(ModelForm):
        model = Discussion
        fields = ['theme', 'post', 'image', 'posted', 'author']
        widgets = {'posted': HiddenInput(), 'author': HiddenInput()}


class DiscussionReactionForm(ModelForm):
    """ For creation reaction on theme in discussion. """

    class Meta(ModelForm):
        model = Discussion
        fields = ['theme', 'post', 'posted', 'author', 'reaction']
        widgets = {'theme': HiddenInput(), 'posted': HiddenInput(), 'author': HiddenInput(), 'reaction': HiddenInput()}
