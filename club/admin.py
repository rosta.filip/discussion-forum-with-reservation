from django.contrib import admin
from django.contrib.admin import ModelAdmin, TabularInline

# Register your models here.
from club.models import Actuality, Discussion


class ActualityAdmin(ModelAdmin):
    list_display = ('topic', 'story', 'image', 'created', 'modified', 'author')


class DiscussionAdmin(ModelAdmin):
    list_display = ('theme', 'post', 'image', 'posted', 'modified', 'author', 'reaction')


admin.site.register(Actuality, ActualityAdmin)
admin.site.register(Discussion, DiscussionAdmin)
