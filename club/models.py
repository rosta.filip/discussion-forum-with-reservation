from django.db.models import CharField, DateTimeField, TextField, ImageField, ForeignKey, Model, PROTECT

from accounts.models import User

# Create your models here.


class Actuality(Model):
    topic = CharField(max_length=512, null=False, blank=False)
    story = TextField(null=False, blank=False)
    image = ImageField(upload_to='actuality', null=True, blank=True)
    created = DateTimeField(null=False, blank=False)
    modified = DateTimeField(auto_now=True)
    author = ForeignKey(User, null=False, on_delete=PROTECT, related_name='actualities')

    def __str__(self):
        return f'{self.topic} [{self.created}]'


class Discussion(Model):
    theme = CharField(max_length=512, null=False, blank=False)
    post = TextField(null=False, blank=False)
    image = ImageField(upload_to='discussion', null=True, blank=True)
    posted = DateTimeField(null=False, blank=False)
    modified = DateTimeField(auto_now=True)
    author = ForeignKey(User, null=False, on_delete=PROTECT, related_name='discussions')
    reaction = ForeignKey('Discussion', on_delete=PROTECT, null=True, blank=True)  # dle generické relace

    def __str__(self):
        return f'{self.theme} [{self.posted}]'
