"""" Defining URL patterns for registration. """

from django.urls import path
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView

from accounts.views import SignUpView, MyAccountView, SignUpDetailView, MyAccountDetailView

app_name = 'accounts'

urlpatterns = [
    # Sign Up
    path('registration/', SignUpView.as_view(), name='registration'),
    # Sign Up detail
    path('registration_detail/<int:id>/', SignUpDetailView.as_view(), name='registration_detail'),

    # Account edit
    path('my_account/', MyAccountView.as_view(), name='my_account'),
    # Account detail edit
    path('my_account_detail/', MyAccountDetailView.as_view(), name='my_account_detail'),


    # Password change
    path('password_change/', PasswordChangeView.as_view(template_name='registration/password_change.html'),
         name='password_change'),
    # Password change confirmation
    path('password_change_done/', PasswordChangeDoneView.as_view(template_name='registration/password_change_done.html'),
         name='password_change_done'),
]