from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from accounts.models import ClubMember

# Register your models here.

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class ClubMemberInLine(admin.StackedInline):
    model = ClubMember
    can_delete = False

# Define a new user admin
class UserAdmin(BaseUserAdmin):
    inlines = (ClubMemberInLine, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
