from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from django.forms import EmailField, ModelForm, CharField, DateField, ChoiceField, HiddenInput
from accounts.models import ClubMember


def user_validation(value: str):
    names = User.objects.all()
    for x in names:
        if value.lower() == x.username:
            raise ValidationError('This user name already exists.')
    if len(value.split()) > 1:
        raise ValidationError('User name must contain only one word.')


def first_name_many_words(value: str):
    if len(value.split()) > 1:
        raise ValidationError('First name must contain only one word.')


def proper_phone_number(value: str):
    if value[0] != '+':
        raise ValidationError('Enter the valid number with the prefix')


class UserForm(UserCreationForm):
    """ For creating new user."""
    class Meta(UserCreationForm):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']

    username = CharField(
        min_length=2,
        max_length=64,
        validators=[user_validation],
        required=True,
        label='User name: ')
    first_name = CharField(
        min_length=2,
        max_length=64,
        validators=[first_name_many_words],
        required=True,
        label='First name: ')
    last_name = CharField(min_length=2, max_length=64, required=True, label='Last name: ')
    email = EmailField(max_length=128, required=True, label='Email: ')


class UserDetailForm(ModelForm):
    """ For creating detail information about new user."""
    class Meta(ModelForm):
        model = ClubMember
        fields = ['user', 'date_of_birth', 'gender', 'phone', 'street', 'house_number', 'city', 'zip_code']
        widgets = {'username': HiddenInput()}

    date_of_birth = DateField(required=True, label='Date of birth (YYYY-MM-DD)')
    gender = ChoiceField(choices=[('Male', 'Male'), ('Female', 'Female')], required=True, label='Gender')
    phone = CharField(
        max_length=32,
        validators=[proper_phone_number],
        required=True,
        label='Phone number')
    street = CharField(max_length=64, required=True, label='Street')
    house_number = CharField(max_length=64, required=True, label='House number')
    city = CharField(max_length=64, required=True, label='City')
    zip_code = CharField(max_length=64, required=True, label='ZIP code')


class UserEditForm(ModelForm):
    """ For editing a user information. """
    class Meta(ModelForm):
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        widgets = {'username': HiddenInput()}


class UserEditDetailForm(ModelForm):
    """ For editing detail information about user."""
    class Meta(ModelForm):
        model = ClubMember
        fields = ['user', 'date_of_birth', 'gender', 'phone', 'street', 'house_number', 'city', 'zip_code']
        widgets = {'user': HiddenInput()}

    date_of_birth = DateField(required=True, label='Date of birth: ')
    gender = ChoiceField(choices=[('Male', 'Male'), ('Female', 'Female')], required=True, label='Gender: ')
    phone = CharField(
        max_length=32,
        validators=[proper_phone_number],
        required=True,
        label='Phone number: ')
    street = CharField(max_length=64, required=True, label='Street: ')
    house_number = CharField(max_length=64, required=True, label='House number: ')
    city = CharField(max_length=64, required=True, label='City: ')
    zip_code = CharField(max_length=64, required=True, label='ZIP code: ')
