from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django.views.generic import CreateView, UpdateView
from django.urls import reverse_lazy, reverse
from django.shortcuts import get_object_or_404

from accounts.models import ClubMember
from accounts.forms import UserForm, UserDetailForm, UserEditForm, UserEditDetailForm

# Create your views here.


class SignUpView(CreateView):
    """ Create a new user. """
    template_name = 'registration/signup.html'
    model = User
    form_class = UserForm

    def get_success_url(self):
        return reverse(
            'accounts:registration_detail',
            kwargs={'id': self.object.id})


class SignUpDetailView(CreateView):
    """ Create a detail information about new user. """
    template_name = 'registration/signup_detail.html'
    model = ClubMember
    form_class = UserDetailForm
    success_url = reverse_lazy('login')

    def get_initial(self):
        init_data = super().get_initial()
        init_data['user'] = User.objects.filter(id=self.kwargs['id']).first()
        return init_data


class MyAccountView(UpdateView):
    """ Modify information about user. """
    template_name = 'registration/my_account.html'
    model = User
    form_class = UserEditForm
    success_url = reverse_lazy('club:club')

    def get_object(self, queryset=None):
        user_id = self.request.user.id
        user = User.objects.get(id=user_id)
        return user


class MyAccountDetailView(UpdateView):
    """ Modify detailed information about user. """
    template_name = 'registration/my_account_detail.html'
    model = ClubMember
    form_class = UserEditDetailForm
    success_url = reverse_lazy('accounts:my_account')

    def get_object(self, queryset=None):
        user_id = self.request.user.id
        club_member = ClubMember.objects.get(user=user_id)
        return club_member
