from django.db import models
from django.contrib.auth.models import User

from django.db.models import CharField, DateField, SmallIntegerField

# Create your models here.


class ClubMember(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date_of_birth = DateField(auto_now=False, auto_now_add=False, null=False, blank=False) # YYYY-MM-DD format
    gender = CharField(max_length=8, null=False, blank=False, choices=[('Male', 'Male'), ('Female', 'Female')])
    phone = CharField(max_length=32, default='', null=False, blank=False)
    club_number = SmallIntegerField(null=False, default='0', blank=True)
    street = CharField(max_length=128, null=False, blank=False)
    house_number = CharField(max_length=16, null=False, blank=False)
    city = CharField(max_length=64, null=False, blank=False)
    zip_code = CharField(max_length=16, null=False, blank=False)

    def __str__(self):
        return f'{self.user}'
